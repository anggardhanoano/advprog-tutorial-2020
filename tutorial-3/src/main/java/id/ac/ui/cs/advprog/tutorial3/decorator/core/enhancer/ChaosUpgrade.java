package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;
    Random random;
    int newValue;

    public ChaosUpgrade(Weapon weapon) {

        this.weapon = weapon;
        random = new Random();
        newValue = 50 + random.nextInt(6);
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        if (weapon == null) {
            return newValue;
        }
        return newValue + weapon.getWeaponValue();
    }

    @Override
    public String getDescription() {
        if (weapon == null) {
            return "Chaos";
        }
        return weapon.getDescription();
    }
}
