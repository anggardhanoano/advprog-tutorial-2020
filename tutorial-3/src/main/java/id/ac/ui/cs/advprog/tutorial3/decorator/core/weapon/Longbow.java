package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Longbow extends Weapon {
    public Longbow() {
        this.weaponName = "Longbow";
        this.weaponDescription = "Lovely used " + this.weaponName;
        this.weaponValue = 15;
    }
}
