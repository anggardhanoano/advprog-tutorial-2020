package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {

    String name, role;
    List<Member> child;

    public PremiumMember(String name, String role) {
        this.name = name;
        this.role = role;
        this.child = new ArrayList<Member>();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return this.role;
    }

    @Override
    public void addChildMember(Member member) {
        if (this.role.equals("Master") || this.child.size() < 3) {
            this.child.add(member);
        }

    }

    @Override
    public void removeChildMember(Member member) {
        if (this.child.size() > 0) {
            this.child.remove(member);
        }
    }

    @Override
    public List<Member> getChildMembers() {
        return this.child;
    }
    // TODO: Complete me
}
