package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Weapon weapon;
    Random random;
    int newValue;

    public RegularUpgrade(Weapon weapon) {

        this.weapon = weapon;
        random = new Random();
        newValue = 1 + random.nextInt(5);
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        if (weapon == null) {
            return newValue;
        }
        return newValue + weapon.getWeaponValue();
    }

    @Override
    public String getDescription() {
        if (weapon == null) {
            return "Regular";
        }
        return weapon.getDescription();
    }
}
