package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class MagicUpgrade extends Weapon {

    Weapon weapon;
    Random random;
    int newValue;

    public MagicUpgrade(Weapon weapon) {

        this.weapon = weapon;
        random = new Random();
        newValue = 15 + random.nextInt(6);
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        if (weapon == null) {
            return newValue;
        }
        return newValue + weapon.getWeaponValue();
    }

    @Override
    public String getDescription() {
        if (weapon == null) {
            return "Magic";
        }
        return weapon.getDescription();
    }
}
