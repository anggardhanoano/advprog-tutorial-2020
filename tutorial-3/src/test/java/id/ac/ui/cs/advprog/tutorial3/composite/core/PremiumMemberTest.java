package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName() {
        assertEquals(member.getName(), "Wati");
    }

    @Test
    public void testMethodGetRole() {
        assertEquals(member.getRole(), "Gold Merchant");
    }

    @Test
    public void testMethodAddChildMember() {
        Member newMember = new PremiumMember("Siti", "Gold Merchant");
        member.addChildMember(newMember);
        assertEquals(member.getChildMembers().size(), 1);
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member newMember = new PremiumMember("Siti", "Gold Merchant");
        member.addChildMember(newMember);
        member.removeChildMember(newMember);
        assertEquals(member.getChildMembers().size(), 0);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        Member newMember1 = new PremiumMember("Siti", "Gold Merchant");
        Member newMember2 = new PremiumMember("Sita", "Gold Merchant");
        Member newMember3 = new PremiumMember("Shit", "Gold Merchant");
        Member newMember4 = new PremiumMember("Shiet", "Gold Merchant");
        member.addChildMember(newMember1);
        member.addChildMember(newMember2);
        member.addChildMember(newMember3);
        member.addChildMember(newMember4);
        assertEquals(member.getChildMembers().size(), 3);
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        Member master = new PremiumMember("lord", "Master");
        Member newMember1 = new PremiumMember("Siti", "Gold Merchant");
        Member newMember2 = new PremiumMember("Sita", "Gold Merchant");
        Member newMember3 = new PremiumMember("Shit", "Gold Merchant");
        Member newMember4 = new PremiumMember("Shiet", "Gold Merchant");
        master.addChildMember(newMember1);
        master.addChildMember(newMember2);
        master.addChildMember(newMember3);
        master.addChildMember(newMember4);
        assertEquals(master.getChildMembers().size(), 4);
    }
}
