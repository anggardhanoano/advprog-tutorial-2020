package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShieldTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp() {
        weapon = new Shield();
    }

    @Test
    public void testMethodGetWeaponName() {
        assertEquals(weapon.getName(), "Shield");
    }

    @Test
    public void testMethodGetWeaponDescription() {
        assertEquals(weapon.getDescription(), "Lovely used Shield");
    }

    @Test
    public void testMethodGetWeaponValue() {
        assertEquals(weapon.getWeaponValue(), 10);
    }
}
