package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Nina", "Merchant");
    }

    @Test
    public void testMethodGetName() {
        assertEquals(member.getName(), "Nina");
    }

    @Test
    public void testMethodGetRole() {
        assertEquals(member.getRole(), "Merchant");
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        Member newMember = new OrdinaryMember("Ana", "Artis");
        member.addChildMember(newMember);
        assertEquals(member.getChildMembers().size(), 0);
        member.removeChildMember(newMember);
        assertEquals(member.getChildMembers().size(), 0);
    }
}
