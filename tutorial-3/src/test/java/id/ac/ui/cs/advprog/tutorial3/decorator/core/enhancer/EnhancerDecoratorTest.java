package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnhancerDecoratorTest {

    Weapon weapon1;
    Weapon weapon2;
    Weapon weapon3;
    Weapon weapon4;
    Weapon weapon5;
    int value;

    @Test
    public void testAddWeaponEnhancement() {

        weapon1 = EnhancerDecorator.CHAOS_UPGRADE.addWeaponEnhancement(weapon1);
        weapon2 = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weapon2);
        weapon3 = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weapon3);
        weapon4 = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weapon4);
        weapon5 = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weapon5);

        value = weapon1.getWeaponValue();
        assertTrue(value >= 50 && value <= 55);
        value = weapon2.getWeaponValue();
        assertTrue(value >= 15 && value <= 20);
        value = weapon3.getWeaponValue();
        assertTrue(value >= 1 && value <= 5);
        value = weapon4.getWeaponValue();
        assertTrue(value >= 5 && value <= 10);
        value = weapon5.getWeaponValue();
        assertTrue(value >= 10 && value <= 15);

    }

}
