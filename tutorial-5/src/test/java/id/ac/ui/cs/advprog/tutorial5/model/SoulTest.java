package id.ac.ui.cs.advprog.tutorial5.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import id.ac.ui.cs.advprog.tutorial5.model.*;

public class SoulTest {

    private Soul soul;

    @BeforeEach
    public void setUp() throws Exception {
        this.soul = new Soul(1, "Angga", 20, "M", "Mahasiswa");
    }

    @Test
    public void testGetName() {
        assertEquals("Angga", this.soul.getName());
    }

    @Test
    public void testGetAge() {
        assertEquals(20, this.soul.getAge());
    }

    @Test
    public void testGetGender() {
        assertEquals("M", this.soul.getGender());
    }

    @Test
    public void testGetOccupation() {
        assertEquals("Mahasiswa", soul.getOccupation());
    }

    @Test
    public void testsetName() {
        soul.setName("Tono");
        assertEquals("Tono", soul.getName());
    }

    @Test
    public void testsetGender() {
        soul.setGender("F");
        assertEquals("F", soul.getGender());
    }

    @Test
    public void testsetAge() {
        soul.setAge(17);
        assertEquals(17, soul.getAge());
    }
}