package id.ac.ui.cs.advprog.tutorial5.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import id.ac.ui.cs.advprog.tutorial5.model.Soul;

// TODO: Import Soul.java
public interface SoulRespository extends JpaRepository<Soul, Long> {
}
