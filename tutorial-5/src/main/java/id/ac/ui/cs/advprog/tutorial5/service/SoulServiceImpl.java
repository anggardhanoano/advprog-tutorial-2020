package id.ac.ui.cs.advprog.tutorial5.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.ac.ui.cs.advprog.tutorial5.model.Soul;
import id.ac.ui.cs.advprog.tutorial5.respository.SoulRespository;

@Service
public class SoulServiceImpl implements SoulService {

    @Autowired
    private SoulRespository repo;

    @Override
    public List<Soul> findAll() {
        return repo.findAll();
    }

    @Override
    public Optional<Soul> findSoul(Long id) {
        return repo.findById(id);
    }

    @Override
    public void erase(Long id) {
        repo.deleteById(id);
    }

    @Override
    public Soul rewrite(Soul soul) {
        return repo.save(soul);
    }

    @Override
    public Soul register(Soul soul) {
        return repo.save(soul);
    }

}