package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        // Subscribe
        this.guild = guild;
        this.guild.add(this);
    }

    @Override
    public void update() {
        if (guild.getQuestType().equals("R") || guild.getQuestType().equals("D")) {
            this.getQuests().add(guild.getQuest());
        }
    }

    // ToDo: Complete Me
}
