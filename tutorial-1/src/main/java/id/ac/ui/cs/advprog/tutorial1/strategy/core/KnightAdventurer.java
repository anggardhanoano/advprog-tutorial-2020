package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {

    public KnightAdventurer() {
        attackBehavior = new AttackWithSword();
        defenseBehavior = new DefendWithArmor();
    }

    @Override
    public String getAlias() {
        return "Knight";
    }
    // ToDo: Complete me
}
