package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {

    @Override
    public String getType() {
        return "Spell";
    }

    @Override
    public String attack() {
        return "3500 damage";
    }
    // ToDo: Complete me
}
