package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.BlankSpell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;

import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    private Spell latestSpell;
    boolean lastUndo;

    public ContractSeal() {
        spells = new HashMap<>();
        this.latestSpell = new BlankSpell();
        lastUndo = false;
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        // TODO: Complete Me
        Spell spell = spells.get(spellName);
        latestSpell = spell;
        spell.cast();
        lastUndo = false;
    }

    public void undoSpell() {
        // TODO: Complete Me
        if (!lastUndo) {
            latestSpell.undo();
            lastUndo = true;
        }
    }

    public Collection<Spell> getSpells() {
        return spells.values();
    }
}
