package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritStealthSpell extends HighSpiritSpell {

    public HighSpiritStealthSpell(HighSpirit spirit) {
        super(spirit);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void cast() {
        // TODO Auto-generated method stub
        spirit.stealthStance();
    }

    @Override
    public String spellName() {
        return spirit.getRace() + ":Stealth";
    }
}
