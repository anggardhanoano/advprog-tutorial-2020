package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;
import java.util.List;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    List<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }

    @Override
    public void cast() {
        for (Spell s : spells) {
            s.cast();
        }
    }

    @Override
    public void undo() {
        while (!spells.isEmpty()) {
            spells.remove(spells.size() - 1).undo();
        }
    }
}
