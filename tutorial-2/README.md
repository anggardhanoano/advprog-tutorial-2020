# Tutorial 2

# I can Actually Contract and Fight Alongside Spirits in This World

Setelah pada tutorial sebelumnya (tutorial-1), Anda telah menjalankan amanah untuk membantu **_guild_** dalam mempersiapkan strategi **_adventurer_** dan pembagian **_quest_** menggunakan sihir bernama Spring, dimana sihir ini menggunakan pemahaman Advanced Programming yang Anda miliki. Kali ini di `tutorial-2` kita akan mencoba menggunakan sihir Spring untuk berinteraksi dengan ras halus bernama **_spirit_**.

## (Command Pattern)

---

Dengan menggunakan sihir Spring, anda mencoba membuat sebuah **_contract seal_** yang dapat digunakan untuk mengontrak **_spirit_** dan memberikan instruksi pada **_spirit_** yang dikontrak. Namun, sebagai manusia yang muncul dari dunia dengan peradaban yang lebih maju yang mengenal kode etik, anda tidak ingin kontrak yang anda buat disalahgunakan oleh pihak yang tidak bertanggungjawab. Anda akhirnya menghindari kontak langsung antara **_contract seal_** dengan **_spirit_** dengan membuat perantara berupa **`spell`** yang merupakan satu instruksi spesifik untuk satu **_spirit_**.

Akhirnya, anda mencoba merancang **_contract seal_** anda. Pertama, anda membuat sebuah `interface` berupa **`spell`**, yaitu `Spell.java`. `Interface` ini akan diimplementasikan menjadi beberapa instruksi yang dapat dilaksanakan **_spirit_**. **`Spell`** juga dapat melakukan `undo` instruksi jika diperlukan. **_Perlu diingat, ketika `spell` anda menginstruksikan `undo`, _**spirit**_ akan menjalankan hal terakhir yang mereka lakukan. Terlepas dari `spell` yang menginstruksikan `undo`._**

Langkah selanjutnya adalah merancang **`seal`** berupa `ContractSeal.java` yang akan menyimpan berbagai **`spell`** yang sebelumnya sudah diimplementasikan. **`Seal`** yang anda buat dapat menyimpan beberapa **`spell`**. Karena instruksi `undo` yang masih kurang stabil, anda membatasi penggunaan instruksi `undo` dimana **`seal`** anda tidak memperbolehkan pelaksanaan instruksi `undo` dua kali berturut-turut.

Implementasi **`seal`** anda terbagi menjadi 4 bagian, yaitu **_controller_**, **_core_**, **_repository_**, dan **_service_**.

1. Pada **_controller_** terdapat `SealController.java`, anda diminta untuk melengkapi endpoint `cast` dan `undo` pada **_controller_**.

   ```java
   ...
   ...
   @RequestMapping(path = "/cast", method = RequestMethod.POST)
   public String castSpell(@RequestParam(value = "spellName") String spellName) {
       // TODO: Complete Me
       return "redirect:/seal";
   }

   @RequestMapping(path = "/undo", method = RequestMethod.GET)
   public String undoSpell() {
       // TODO: Complete Me
       return "redirect:/seal";
   }
   ...
   ...
   ```

2. Bagian **_core_** merupakan inti dari pekerjaan anda. **_Core_** terbagi lagi menjadi 2 bagian, yaitu **`spell`** dan **_spirit_**.
   I. Spirit
   Spirit terbagi menjadi 2 kelas, yaitu:

   > > a. `Familiar`. Merupakan **_spirit_** tingkat rendah yang hanya dapat di`activate` atau di`seal`.

   > > b. `High Spirit`. Spirit dengan intelegensi yang lebih tinggi, sehingga mengerti setidaknya kemampuan dasar bertarung seperti `attack`, `defend`, dan `stealth`. Seperti `Familiar`, `High Spirit` juga dapat di`seal`.

   II. Spell
   Ada beberapa implementasi **`spell`** pada bagian ini. Implementasi tersebut dapat dibagi menjadi 4 bagian.

   > > a. `Familiar Spell`. Merupakan **_spirit_** tingkat rendah yang hanya dapat di`activate` atau di`seal`.

   > > b. `High Spirit Spell`. Spirit dengan intelegensi yang lebih tinggi, sehingga mengerti setidaknya kemampuan dasar bertarung seperti `attack`, `defend`, dan `stealth`. Seperti `Familiar`, `High Spirit` juga dapat di`seal`.

   > > c. `Chain Spell`. Gabungan beberapa **`spell`** yang dijalankan secara berurutan. **_Instruksi `undo` pada `spell` ini sedikit berbeda, dimana `chain spell` melakukan `undo` pada setiap `spell` yang menyusunnya dengan urutan terbalik._**.

   > > c. `Blank Spell`. **`Spell`** tanpa instruksi. Cocok untuk bluffing atau distraksi.

3. Bagian **_repository_** merupakan tempat dimana anda merancang **`Contract Seal`** anda. Anda melakukan implementasi sesuai rencana awal.

   ```java
   ...
   ...
   public void castSpell(String spellName) {
       // TODO: Complete Me
   }

   public void undoSpell() {
       // TODO: Complete Me
   }
   ...
   ...
   ```

4. Terakhir, bagian **_service_**. Anda mengimplementasi **_service_** yang dapat digunakan **_controller_** untuk melakukan `cast` atau `undo` pada `Contract Seal`.

   ```java
   @Override
   public void castSpell(String spellName) {
       seal.castSpell(spellName);
   }

   @Override
   public void undoSpell() {
       seal.undoSpell();
   }
   ```

Untuk membantu pengecekan progress perancangan, anda membuat TODO List yang berkaitan dengan implementasi **`Contract Seal`** ini.

### TODO List Contract Seal

- [x] Mengimplementasikan semua implementasi `Spell.java`.
- [x] Mengimplementasi `cast` dan `undo` pada `ContractSeal.java`.
- [x] Mengimplementasi `cast` dan `undo` pada `SealServiceImpl.java`.
- [x] Mengimplementasi `cast` dan `undo` pada `SealController.java`.

## (Template Pattern)

---

Setelah, para **_adventurer_** melakukan **_contract seal_** dengan **_spirit_** , selanjutnya terdapat **_quest_** yang harus dijalankan para **_adventurer_**. **_adventurer_** memiliki **_skill_** spesial pada **_quest_** dan menggunakan **_spirit_** yang akan membantu mereka menjalankan **_quest_**.

Disini para **_spirit_** akan melakukan **_movement_** secara spesifik sesuai dengan jenisnya. Terdapat 3 jenis spirit, yaitu : Saber, Archer, Lancer yang siap membantu **_adventurer_**. Perbedaan dari ketiga jenis spirit tersebut ada pada **_movement_** buff dan akan menambahkan **_damage_** kepada **_movement_** selanjutnya. Untuk membantu **_adventurer_** kalian disini menentukan urutan **_movement_** pada **_spirit_**.

### TODO List Template

- [x] Mengimplementasikan pola **_movement_** setiap spirit pada `SpiritInQuest.java`

## Notes

Tugas kalian pada 2 Pattern diatas adalah melengkapi tiap implementasi yang ditandai oleh `//TODO: Complete me`.
